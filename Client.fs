﻿namespace WSClientAsync

open WebSharper
open WebSharper.UI
open WebSharper.UI.Templating
open WebSharper.UI.Notation
open WebSharper.Sitelets
open WebSharper.UI
open WebSharper.UI.Client
open WebSharper.UI.Html
open WebSharper.UI.Html.Tags
[<JavaScript>]
module Templates =

    type MainTemplate = Templating.Template<"Main.html", ClientLoad.FromDocument, ServerLoad.WhenChanged>

[<JavaScript>]
module Routing =
   let router : WebSharper.Sitelets.Router<EndPoint> =
        WebSharper.Sitelets.InferRouter.Router.Infer ()
   // client side routing only for home and about
   let installedRouter =
        router
        |> Router.Slice
            (fun endpoint ->
                match endpoint with
                | Home _-> Some endpoint
                | About -> Some endpoint
                | _ -> None
            )
            id
        |> WebSharper.UI.Router.Install (Home 4)

[<JavaScript>]
module Client =

    Routing.installedRouter.View
    |> View.Sink (function
        |About -> JavaScript.JS.Document.Title <- "About"
        |Home i -> JavaScript.JS.Document.Title <- $"Home {i}"
        |Root ->  JavaScript.JS.Document.Title <- "/"
    )

    let displayIntegers (integers: int list) =
        integers |> List.map (string >> text) |> Doc.Concat

    let clientBody () =
        async {
            let view =
                Routing.installedRouter.View
                |> View.MapAsync
                    (function
                        |About -> async { return div [] [text "home body from client side routing"] }
                        |Home length ->
                            async {
                                let! integers = Server.getIntegers length
                                return div [] [
                                                displayIntegers integers
                                                ]
                            }
                        |Root ->
                            async {
                                let! integers = Server.getIntegers 6
                                return div [] [
                                                displayIntegers integers
                                                ]
                            }
                        | _  ->  async { return div [] [text "not implemented"] }
                    )
            return view |> Client.Doc.EmbedView
        } |> WebSharper.UI.Client.Doc.Async
