﻿namespace WSClientAsync

open WebSharper

type EndPoint =
    | [<EndPoint "/">] Root
    | [<EndPoint "/">] Home of int
    | [<EndPoint "/about">] About

module Server =

    [<Rpc>]
    let DoSomething input =
        let R (s: string) = System.String(Array.rev(s.ToCharArray()))
        async {
            return R input
        }
    [<Rpc>]
    let getIntegers length =
        async {
            return (List.init length id)
        }
