namespace WSClientAsync

open WebSharper
open WebSharper.Sitelets
open WebSharper.UI
open WebSharper.UI.Server

module Templating =
    open WebSharper.UI.Html

    // Compute a menubar where the menu item for the given endpoint is active
    let MenuBar (ctx: Context<EndPoint>) : Doc list =
        let ( => ) txt act =
             li [] [
                a [attr.href (ctx.Link act)] [text txt]
             ]
        [
            "Root" => EndPoint.Root
            "Home" => EndPoint.Home 3
            "Home2" => EndPoint.Home 2
            "About" => EndPoint.About
        ]

    let Main ctx (body: Doc list) =
        Content.Page(
            Templates.MainTemplate()
                .MenuBar(MenuBar ctx)
                .Body(body)
                .Doc()
        )

module Site =
    open WebSharper.UI.Html

    let HomePage ctx =
        let body = client <@ Client.clientBody () @>
        Templating.Main ctx [
            h1 [] [text "we navigated to server side generated page Home"]
            div [] [ body ]
        ]

    let AboutPage ctx =
        let body = client <@ Client.clientBody () @>
        Templating.Main ctx [
            h1 [] [text "we navigated to server side generated page About"]
            body
        ]

    [<Website>]
    let Main =
        Application.MultiPage (fun ctx endpoint ->
            match endpoint with
            | EndPoint.Home _ -> HomePage ctx
            | EndPoint.About -> AboutPage ctx
            | EndPoint.Root -> HomePage ctx
        )
